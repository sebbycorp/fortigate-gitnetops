---
weight: 60
title: "Ansible Outbound Access"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


## Let's create outbound access

Next we need to create a output policy/rule on our fortigate firewall to allow output access. 

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible2.png?ref_type=heads)


## Step 1. Create Next playbook 

Create a new file called 01-playbook.yml using vi or vscode and create the following playbook.

```bash
- hosts: fortigates
  connection: httpapi
  collections:
  - fortinet.fortios
  vars:
   ansible_httpapi_use_ssl: yes
   ansible_httpapi_validate_certs: no
   ansible_httpapi_port: 443

  vars_files:
    - secrets.yml

  tasks:
      - name: Create Firewall Policy for Outbound Traffic
        fortios_firewall_policy:
          vdom: "{{ vdom }}"
          access_token: "{{ fortigate_token }}"
          state: "present"
          firewall_policy:
            policyid: 1  # Adjust policy ID as needed
            name: "oubtound_traffic"
            srcintf: 
              - name: "awsgeneve"  # Specify the source interface
            dstintf: 
              - name: "awsgeneve"  # Specify the destination interface
            srcaddr: 
              - name: "az1_subnet"
            dstaddr: 
              - name: "all"
            service: 
              - name: "ALL"
            action: "accept"
            schedule: "always"
            logtraffic: "all"
```

This Ansible playbook is designed to automate the configuration of firewall policies on FortiGate devices using the httpapi connection. It specifically creates a firewall policy for managing outbound traffic through a FortiGate firewall. Here's a detailed breakdown of its components:

Hosts: The playbook targets the fortigates group, indicating it is intended to run on devices classified under this group in the Ansible inventory, which are presumably FortiGate firewalls.

Connection: Utilizes the httpapi connection type, suggesting that the playbook communicates with the FortiGate devices using their HTTP API, a common approach for automating tasks on network devices that support API interactions.

Collections: Employs the fortinet.fortios collection, which includes Ansible modules specifically designed for managing Fortinet's FortiOS, the operating system running on FortiGate devices.

Vars:

ansible_httpapi_use_ssl: Set to yes to ensure the connection to the device is encrypted using SSL (HTTPS).
ansible_httpapi_validate_certs: Disabled (set to no) to bypass SSL certificate validation. This is useful in environments where devices might use self-signed certificates.
ansible_httpapi_port: Specifies the port for the HTTPS connection, using the standard secure port 443.
Vars_files: Includes external variable definitions from secrets.yml, which likely contains sensitive information such as the vdom (virtual domain on the FortiGate to manage) and fortigate_token (the access token for API authentication).

Tasks: Defines a single task to create or ensure the presence of a specific firewall policy for outbound traffic.

Name: "Create Firewall Policy for Outbound Traffic"
Module: fortios_firewall_policy, part of the fortinet.fortios collection, used here to manage firewall policies on the device.
Parameters:
vdom: Specifies the virtual domain within the FortiGate to which the policy will be applied, leveraging a variable that should be defined in secrets.yml.
access_token: Uses a variable for authentication, expected to be defined in the external variables file.
state: Set to "present", ensuring the firewall policy exists with the specified settings.
firewall_policy: Details of the policy to be applied, including:
policyid: The unique identifier for the policy. This needs to be adjusted based on existing policies to avoid conflicts.
name: The name of the policy, here described as "outbound_traffic".
srcintf and dstintf: Define the source and destination interfaces for the traffic, both set to "awsgeneve" in this example.
srcaddr and dstaddr: Specify the source and destination addresses for the policy, targeting "az1_subnet" for source and "all" for destination addresses.
service: The services covered by the policy, set to "ALL" to include all traffic.
action: Defines what to do with the matched traffic, here set to "accept".
schedule: When the policy is active, set to "always" in this case.
logtraffic: Determines the logging level for traffic that matches the policy, set to log all traffic.
This playbook illustrates how Ansible can be used to streamline and automate the management of network security policies on FortiGate firewalls, enhancing efficiency and ensuring consistency across the network infrastructure.

## Execute the playbook 

Exectute the following playbook and insert your ansible-vault password

```bash
ansible-playbook -i hosts.ini 01-playbook.yml --ask-vault-pass
```

The ouptu will look like this, validate the policy has been deployed

```

PLAY [fortigates] ********************************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************
ok: [awsfortigate]

TASK [Create Firewall Policy for Outbound Traffic] ***********************************************************************************************************
changed: [awsfortigate]

PLAY RECAP ***************************************************************************************************************************************************
awsfortigate               : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```

...