---
weight: 50
title: "Ansible First Playbook"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


# Let's deploy your first playbook

The exising environment is deploy and if you log into your foritgate you will notice there are no rules and policies built. So the first step we will need to do is to deploy a an address group that with the az1 subnet.

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible1.png?ref_type=heads)

## Step 1. Create first playbook 

Create a new file called 00-playbook.yml using vi or vscode and create the following playbook.

```bash
- hosts: fortigates
  connection: httpapi
  collections:
  - fortinet.fortios
  vars:
   ansible_httpapi_use_ssl: yes
   ansible_httpapi_validate_certs: no
   ansible_httpapi_port: 443

  vars_files:
    - secrets.yml

  tasks:
      - name: Define a AWS AZ1 Address Group
        fortios_firewall_address:
          vdom: "{{ vdom }}"
          access_token: "{{ fortigate_token }}"
          state: "present"
          firewall_address:
            name: "az1_subnet"
            subnet: "20.1.1.0 255.255.255.0"
            comment: "Created by Ansible" 
```

The YAML snippet you've shared describes an Ansible playbook that is designed to configure FortiGate devices using the httpapi connection method and the fortinet.fortios collection. This playbook specifically aims to define an AWS Availability Zone 1 (AZ1) address group on a FortiGate device. Here's a breakdown of the key components of the playbook:

Hosts: It targets hosts grouped under fortigates, indicating that the operations are intended for devices identified as FortiGate firewalls.

Connection: The playbook uses the httpapi connection type, which is suitable for devices that can be managed over HTTP or HTTPS APIs, indicating that it communicates with FortiGate devices through their HTTP-based API.

Collections: It specifies the use of the fortinet.fortios collection, a set of Ansible modules provided by Fortinet that are specifically designed to manage FortiOS devices, including FortiGate firewalls.

Vars:

ansible_httpapi_use_ssl: This variable is set to yes, indicating that the playbook should use SSL (HTTPS) for the API connection.
ansible_httpapi_validate_certs: Set to no, this tells Ansible not to validate SSL certificates. This might be used in environments where self-signed or untrusted certificates are in place.
ansible_httpapi_port: Specifies the port to connect to, set to 443, which is the standard port for HTTPS.
Vars_files: The playbook includes a variable file named secrets.yml. This file likely contains sensitive information such as authentication tokens (fortigate_token) or other secrets required for the playbook to execute securely.

Tasks: The playbook defines a single task:

Name: "Define a AWS AZ1 Address Group"
Module: fortios_firewall_address, a module from the fortinet.fortios collection used to manage firewall address objects in FortiOS.
Parameters:
vdom: The virtual domain on the FortiGate device to which the configuration applies, specified by a variable {{ vdom }} that would be defined elsewhere in the playbook or in the secrets.yml file.
access_token: Authentication token required for API access, specified by {{ fortigate_token }}.
state: Set to "present", ensuring that the address object exists with the given configuration.
firewall_address: The details of the firewall address object to be created or updated, including its name (az1_subnet), subnet (20.1.1.0 255.255.255.0), and a comment.
This playbook is a straightforward example of how Ansible can automate network configuration tasks for FortiGate firewalls, leveraging Fortinet's Ansible collections for secure and efficient management of firewall policies and objects.

## Execute the playbook 

Exectute the following playbook and insert your ansible-vault password

```bash
ansible-playbook -i hosts.ini 00-playbook.yml --ask-vault-pass
```

The ouptu will look like this, validate the addreses group was created.

```
PLAY [fortigates] *******************************************************************************************

TASK [Gathering Facts] **************************************************************************************
ok: [awsfortigate]

TASK [Define a AWS AZ1 Address Group] ***********************************************************************
changed: [awsfortigate]

PLAY RECAP **************************************************************************************************
awsfortigate               : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

...