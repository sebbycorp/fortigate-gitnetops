---
weight: 40
title: "Ansible Inventory"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


## Let's build an inventory
An Ansible inventory is a configuration file that provides information about the machines that Ansible manages. It defines a list of nodes or hosts with their respective IP addresses, domain names, and other parameters that Ansible uses to connect to and interact with them.

Here are some key aspects of an Ansible inventory:

Hosts: Individual machines managed by Ansible. Hosts can be specified with IP addresses or hostnames.
Groups: Collections of hosts that share a common attribute, which makes it easier to target them with specific playbooks. Groups can have children and can also be nested.

Host Variables: Variables specific to a host, which can override group variables for that host.
Group Variables: Variables that are applied to all hosts in a group.

Prepare host inventory using the info from the output.

create a vi hosts.ini 

```
[fortigates]
awsfortigate ansible_host=<replace> fortios_access_token=secrets.yml

[fortigates:vars]
ansible_network_os=fortinet.fortios.fortios
vdom=FG-traffic
fortigate_token=secrets.yml
```


...