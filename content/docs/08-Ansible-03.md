---
weight: 70
title: "Deploy JuiceShop App"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


## Let's deploy an app

With output access deployed, we can now deploy a juicebox application. 

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible3.png?ref_type=heads)

## Steps to deploy application in AWS

* jump into the terraform folder and edit the main.tf file
* uncomment the following code and save the file

```bash
# module "juiceshop" {
#   source = "./juiceshop"
#   fwsshkey           = module.infrastructure.fwsshkey
#   customer_vpc_id    = module.infrastructure.customer_vpc_id
#   csprivatesubnetaz1 = module.infrastructure.csprivatesubnetaz1
#   depends_on = [ module.infrastructure]
# }

#output "juiceshop_public_ip" {  
#  value = module.juiceshop.juiceshop_public_ip  
#}  

#output "juiceshop_private_ip" {  
#  value = module.juiceshop.juiceshop_private_ip  
#}  `
```

* next execute the following command to deploy the application 

```bash
./deploy.sh
```

The code will deploy an application in your environment and the output will look like the following. 

```bash
CustomerVPC = "vpc-08a6fba32e8484bfb"
FGTPublicIP = "54.217.65.12"
FGTVPC = "vpc-0a2cb5b6a9eba2a22"
LoadBalancerPrivateIP = "10.1.1.157"
Password = "i-0433533d74b68c24b"
Username = "admin"
csprivatesubnetaz1 = "subnet-0ce4ec9c8c9076290"
csprivatesubnetaz2 = "subnet-0926575924429120e"
customer_vpc_id = "vpc-08a6fba32e8484bfb"
fwsshkey = "sshfwkey"
juiceshop_private_ip = "20.1.1.185"
juiceshop_public_ip = "34.253.206.152"
```

## Validate

You can log into the foritgate and validate access to the web app on the juiceshop_public_ip public ip address. Access will be denied becuase we do not have a policy that allows external users to acces the service. So let's move forward and create a playbook to allow access.

...