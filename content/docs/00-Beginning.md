---
weight: 1
title: "Beginning"
description: ""
icon: "article"
date: "2023-12-22T14:06:28-05:00"
lastmod: "2023-13-22T14:06:28-05:00"
draft: false
toc: false
---

## Deploy in your own environment. 


If you want to deploy this fortigate ansible playground using your own terminal server deploy the following docker-compose.yaml docker file. 

## Requirements
* Docker installed

## Option 1 Download docker-compose file

Download the following docker-file 
```bash
wget https://raw.githubusercontent.com/maniak-academy/aws-lab-getting-started-with-Ansible-Fortigate/main/docker-compose.yaml
```
## Option 2 Create a docker compose file 

Create a file called docker-compose.yaml in your directory 

```bash
version: '3.8'

services:
     
  terminal-server:
    image: sebbycorp/terminal-server:latest
    ports:
      - "7681:7681"
    environment:
      - TINI_KILL_PROCESS_GROUP=1
    # volumes:
    #   - /your/local/workspace:/workspace
    entrypoint: ["/sbin/tini", "--"]
    command: ["ttyd", "-s", "3", "-t", "titleFixed=/bin/sh", "-t", "rendererType=webgl", "-t", "disableLeaveAlert=true", "/bin/sh", "-i", "-l"]
```


## Step 2 Execute the docker file

```
docker-compose up -d
```



...