---
weight: 80
title: "Allow Inbound Access"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


## Create a playbook to allow access

With the application online, it's time to allow access to the web application. To achieve this we need to the following.
* Create an address group with the application ip
* Create a policy to allow http access to the webserver

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible4.png?ref_type=heads)

## Create the playbook

## Step 1. Create Next playbook 

* Create a new file called 02-playbook.yml using vi or vscode and create the following playbook.
* Make edits to the playbook that include your private IP address

```bash
- hosts: fortigates
  connection: httpapi
  collections:
  - fortinet.fortios
  vars:
   ansible_httpapi_use_ssl: yes
   ansible_httpapi_validate_certs: no
   ansible_httpapi_port: 443

  vars_files:
    - secrets.yml

  tasks:
      - name: Define a Juiceshop Web Address
        fortios_firewall_address:
          vdom: "{{ vdom }}"
          access_token: "{{ fortigate_token }}"
          state: "present"
          firewall_address:
            name: "web-add-group"
            subnet: "20.1.1.185 255.255.255.255"
            comment: "Created by Ansible"
            
      - name: Create Firewall Policy for JucieShop
        fortios_firewall_policy:
          vdom: "{{ vdom }}"
          access_token: "{{ fortigate_token }}"
          state: "present"
          firewall_policy:
            policyid: 2  # Adjust policy ID as needed
            name: "inbound_juiceshop"
            srcintf: 
              - name: "awsgeneve"  # Specify the source interface
            dstintf: 
              - name: "awsgeneve"  # Specify the destination interface
            srcaddr: 
              - name: "all"
            dstaddr: 
              - name: "web-add-group"
            service: 
              - name: "HTTP"
            action: "accept"
            schedule: "always"
            logtraffic: "all"
```

## Step 2. Execute the configuration


```bash
ansible-playbook -i hosts.ini 02-playbook.yml --ask-vault-pass
```

## Step 3. Try to access the webapp
* Validate the deployment, try to hit the webapp using the public ip address. 
* Log into the fortigate firewall and take a look at the firewall policy to see what was built and to see the traffic counter when you visit the webapp

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible5.png?ref_type=heads)

...