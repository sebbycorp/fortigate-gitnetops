---
weight: 20
title: "Deploy lab"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---

## Installing Ansible on Ubuntu: A Hands-On Lab

Participants will learn how to install and configure Ansible on an Ubuntu server, gaining a fundamental understanding of Ansible installation and basic configurations.

## Required Materials
- Access to an Ubuntu server (physical or virtual)
- Internet connectivity for the Ubuntu server
- Terminal access (SSH access if using a remote server)

## Tutorial Outline

### Step 1: Introduction to the Lab
- Brief overview of Ansible and its uses.
- Explanation of the lab setup and objectives.

### Step 2: Preparing the Ubuntu Server
Ensure the server is updated:

```bash
sudo apt update && sudo apt upgrade -y
```

Install software-properties-common if not already installed:

```bash
sudo apt install software-properties-common
```

### Step 3: Installing Ansible

Add Ansible’s official PPA (Personal Package Archive) to the system:

```bash
sudo add-apt-repository --yes --update ppa:ansible/ansible
```

Install Ansible:

```bash
sudo apt install ansible -y
```

Verify the installation:

```bash
ansible --version
```

Install FortiOS Galaxy Collection The FortiOS Ansible Galaxy supports multilple FortiOS major releases, you can install the latest collection by default via command

```bash
ansible-galaxy collection install fortinet.fortios
```

### Step 4: Basic Configuration of Ansible

Create a simple inventory file. Explain the concept of inventory in Ansible:

```bash
echo 'localhost ansible_connection=local' > ~/ansible_hosts
```

```bash
Set the environment variable for the inventory file
```

``` bash
export ANSIBLE_INVENTORY=~/ansible_hosts
```

Test Ansible setup with a ping module:

```bash
ansible all -m ping
```

### Step 5: Hands-On Exercise [bonus]
Instruct participants to write a basic playbook to automate a simple task on their server (e.g., creating a directory).
Guide them through running the playbook.

Ceate a new file named create_directory.yml. This can be done using a text editor or directly in the terminal:


```
vi create_directory.yml
```

Step 1. Write the Playbook:
Instruct them to write the following content into the create_directory.yml file:


```yaml
---
- name: Create a directory using Ansible
  hosts: localhost
  become: yes
  tasks:
    - name: Ensure the directory exists
      ansible.builtin.file:
        path: "/path/to/your/directory"
        state: directory
        mode: '0755'
```


Explain the components:

--- signifies the start of YAML.
name: A human-readable description of the playbook.
hosts: Defines on which hosts to run the tasks. Here, it's set to localhost.
become: yes: Ensures that tasks are run with elevated privileges (sudo).
tasks: The list of tasks to be executed.
The task uses the ansible.builtin.file module to ensure a directory exists at the specified path.

Steo 2. Run the Playbook:
Guide them to run the playbook with the following command:

```bash
ansible-playbook create_directory.yml
```

Verify the Directory Creation:
Instruct them to check if the directory was created successfully on their server:


```bash
ls -l /path/to/your/directory
```


Tips
Encourage participants to experiment with different directory paths and permissions.
Discuss potential errors and how to troubleshoot them, like permission issues or typos in the playbook.
Conclusion
This exercise demonstrates a basic use of Ansible for automating simple tasks on a server. It gives participants hands-on experience with writing and running an Ansible playbook.

Remember to replace /path/to/your/directory with the actual path where they want the directory to be created. This hands-on exercise will help solidify their understanding of how Ansible playbooks are structured and executed.



...