---
weight: 90
title: "Ansible Templates"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


## Ansible Jinja2 Templates

Ansible utilizes Jinja2 templating to enable dynamic expressions and access to variables. Jinja2 is a modern and designer-friendly templating language for Python, modeled after Django's templates. It's widely used in configuration management and is a fundamental tool in Ansible for creating dynamic, flexible playbook and role configurations.

## Key Features of Jinja2 in Ansible

- **Variable Substitution**: Easily substitute variables in your templates. For example, `{{ variable_name }}` would be replaced with the value of `variable_name`.
- **Control Structures**: Incorporate control structures like loops and conditionals. For example, `for` loops can iterate over lists, and `if` statements can conditionally include or exclude parts of the template.
- **Filters**: Transform the output of expressions with filters. For instance, you can use the `default` filter to provide a default value if a variable is undefined.
- **Template Inheritance**: Use template inheritance to build a base "skeleton" template that contains all the common elements and defines blocks that child templates can override.

## Why Use Ansible Jinja2 Templating?

1. **Flexibility**: Jinja2 templates allow the creation of flexible configurations that adapt to the context of deployment environments, different hosts, and other variable data.
2. **Code Reusability**: Templates help in reusing code. Common configurations can be written once in a template and then reused, reducing errors and saving time.
3. **Simplicity**: Jinja2 syntax is readable and easy to understand, making your Ansible playbooks and roles more maintainable.
4. **Dynamic Configuration Files**: Generate dynamic configuration files for applications and services, which is especially handy in environments with multiple servers or varying parameters.
5. **Conditional Expressions**: Easily manage complex configurations with conditional expressions, adapting the configuration based on the specific circumstances of the deployment.

## Example Usage

```yaml
- name: Configure Apache
  template:
    src: /templates/httpd.conf.j2
    dest: /etc/httpd/conf/httpd.conf
```


## Let's build a template

Automation is here to make our lives easier. Therefore, if we wish to continue managing address groups and policies using Ansible, we should aim to build templates that can be reused, with our best practices integrated. Let's templatize the deployment of a new address group.

* We are adding a new subnet and want to provide internet access to these hosts.
* We will built a ansible-playbook and jinja2 template to achieve this.

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible6.png?ref_type=heads)

## Create the fortigate_config.j2 template 

Create new file called fortigate_config.j2 and paste the following.

```bash
- name: Build address Group
  fortios_firewall_address:
    vdom: "{{ vdom }}"
    access_token: "{{ fortigate_token }}"
    state: "present"
    firewall_address:
      name: "{{ address_object_name }}"
      subnet: "{{ address_object_subnet }}"
      comment: "Created by Ansible"

- name: Create Firewall Policy 
  fortios_firewall_policy:
    vdom: "{{ vdom }}"
    access_token: "{{ fortigate_token }}"
    state: "present"
    firewall_policy:
      policyid: "{{ policy_id }}"
      name: "{{ policy_name }}"
      srcintf: 
        - name: "awsgeneve"
      dstintf: 
        - name: "awsgeneve"
      srcaddr: 
        - name: "all"
      dstaddr: 
        - name: "{{ address_object_name }}"
      service: 
        - name: "{{ service_name }}"
      action: "{{ action }}"
      schedule: "{{ schedule }}"
      logtraffic: "{{ log_traffic }}"
```

## Step 2. Create the playbook

* Create a new playbook called 03-playbook.yml with the following variables

```bash
- hosts: fortigates
  connection: httpapi
  collections:
  - fortinet.fortios

  vars_files:
    - secrets.yml

  vars:
    ansible_httpapi_use_ssl: yes
    ansible_httpapi_validate_certs: no
    ansible_httpapi_port: 443
    address_object_name: "az2_subnet"
    address_object_subnet: "20.1.3.0 255.255.255.0"
    policy_id: "4"
    policy_name: "inbound_az2"
    service_name: "ALL"
    action: "accept"
    schedule: "always"
    log_traffic: "all"

  tasks:
    - name: Apply FortiGate Configuration
      include_tasks: fortigate_config.j2
```


## Step 3. Execute the configuration


```bash
ansible-playbook -i hosts.ini 03-playbook.yml --ask-vault-pass
```

## Step 4. Validate the deployment
* log into the firewall and find the address groups and policies deployed. 

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible6.png?ref_type=heads)

...