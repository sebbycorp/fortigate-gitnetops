---
weight: 2
title: "Overview"
description: ""
icon: "article"
date: "2023-12-22T14:06:28-05:00"
lastmod: "2023-13-22T14:06:28-05:00"
draft: false
toc: false
---

## Managing Foritgate With Ansible in AWS - A Comprehensive Introduction

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/env.png?ref_type=heads)

Welcome to our exciting course on building a Foritgate Ansible Lab in Amazon Web Services (AWS)! In this comprehensive introduction, we will explore the intricacies of using Ansible with Fortinet, an industry-leading cybersecurity solution, and how it can be seamlessly integrated into the AWS cloud environment using Ansible.

Throughout this course, you will deploy a secure VPC with GWLBN that will route all traffic throught the fortigate firewall in the central VPC. 


## Why Automate with Ansible and Fortinet

Customers are increasingly compelled to minimize their IT expenditures, augment their operational capacity, and achieve greater results with limited resources. Integrating the capabilities of Ansible with the robust security solutions of Fortinet, and further enhanced by the added benefits of Arrow, can significantly diminish or even eradicate operational delays. This synergy ensures compliance with organizational standards and the adoption of recognized best practices, enabling a more efficient and streamlined IT operation.

## Ansible 

Ansible is an open-source, cross-platform tool for resource provisioning automation that DevOps professionals use for continuous delivery of software code by taking advantage of an "infrastructure as code" approach. It's like a digital butler for your computer systems, making sure everything is set up and running smoothly. It's like a digital butler for your computer systems, making sure everything is set up and running smoothly.

With Ansible, you can automate tasks such as server configuration, application deployment, and infrastructure provisioning. It's like having a personal assistant for your IT needs.

In the context of Fortinet and AWS, Ansible can be used to automate the deployment and management of Fortinet's security solutions within an AWS environment. This can help streamline the process of setting up and maintaining a secure infrastructure, making it easier to keep your digital fortress in tip-top shape.

## Arrow Gets Red hat
Arrow can help you centralize and control your Ansible infrastructure with a visual dashboard, role-based access control, job scheduling, and graphical inventory management. A REST API and CLI make it easy to embed Ansible Automation Controller into existing tools and processes. Red Hat Ansible Automation Platform is the foundation for building and operating automation services at scale, providing enterprises a composable, collaborative, and trusted execution environment. It meets customers where they are in their automation journey, bringing them a flexible automation platform to facilitate success from multiple points in their IT infrastructure. Ansible’s simple, easy-to-read automation language has made it easy for teams across an organization to share, vet, and manage automation content. 

## Our Approach 

At the intersection of innovation and practical application, Arrow offers unique, hands-on immersion labs centered around Ansible. Instead of traditional training, these four-hour, cost-effective sessions present real-world use cases across various technology domains. By simulating actual Day1 - Day2 operations on the platform, we aim to inspire and engage your internal engineers, helping them envision and understand the potential applications of the technology. This interactive experience serves as a catalyst for technology adoption, highlighting Ansible's capabilities and empowering your team to leverage it effectively within your unique organizational context.

...