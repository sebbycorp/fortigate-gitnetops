---
weight: 30
title: "Setup Fortigate API"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


## Configure Fortigate API with Ansible Vault
![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/env.png?ref_type=heads)


## Setup Fortigate Access token

The output of your environment will generate you all the information. Here is an exmaple of the output.

```
CustomerVPC = "vpc-08a6fba32e8484bfb"
FGTPublicIP = "54.217.65.12"
FGTVPC = "vpc-0a2cb5b6a9eba2a22"
LoadBalancerPrivateIP = "10.1.1.157"
Password = "i-0433533d74b68c24b"
Username = "admin"
csprivatesubnetaz1 = "subnet-0ce4ec9c8c9076290"
csprivatesubnetaz2 = "subnet-0926575924429120e"
customer_vpc_id = "vpc-08a6fba32e8484bfb"
fwsshkey = "sshfwkey"
```

1. Log into the fortigate devices https://54.217.65.12
2. Log in with username admin passoword i-0433533d74b68c24b. [note you will need to change the password]
3. System > Admin Profiles > Create New > Profile
4. System > Administrators > Create New rest API

![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible-profile.png?ref_type=heads)
![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible-admin.png?ref_type=heads)
![alternative text](https://gitlab.com/sebbycorp/fortigate-gitnetops/-/raw/main/content/docs/ansible-key.png?ref_type=heads)


## Secrets... Ansible Vault
Ansible Vault is a feature of Ansible that allows you to keep sensitive data such as passwords or keys in encrypted files, rather than as plaintext in playbooks or roles. This is useful when you need to store your configuration data securely and share it across your team or systems.

Here are some key points about Ansible Vault:

Encryption: Vault can encrypt any structured data file used by Ansible. This could be the main inventory file, variable files, or role data files.

Passwords: When you encrypt a file with Ansible Vault, you must provide a password. That password will be required later to edit, view, or decrypt the encrypted data.

Files: Ansible Vault can encrypt entire files, a practice that's useful for files containing variables or configuration that include sensitive information. It's also possible to encrypt only certain variables within a file.

Commands:

ansible-vault create: Create a new encrypted data file.
ansible-vault edit: Edit an encrypted data file. This command will decrypt the file, launch an editor, and re-encrypt the file when the editor is closed.
ansible-vault view: View encrypted file contents without editing them.
ansible-vault decrypt: Decrypt a file for editing or adding to source control.
ansible-vault encrypt: Encrypt a previously unencrypted file.
ansible-vault rekey: Change the password on an encrypted file.
Running Playbooks: When you run a playbook that includes Vault-encrypted files, you need to provide the vault password. This can be done through a command-line prompt, a password file, or other methods like Ansible Tower's built-in vault.

Best Practices: It's recommended to use ansible-vault for sensitive variables and to keep vault files as small as possible. You should only store sensitive information in these files and use regular Ansible variables and files for non-sensitive data.

Ansible Vault is a critical tool for managing secrets within Ansible, providing a balance between security and convenience.

## Create a Vault Secret token with ur API key
Let's make sure we encrypt the token, so lets use Ansible Vault

1. Create the Ansible Vault Secret Token
2. First, you need to encrypt your Fortigate token (47dwqz9pw6GHy6fbmb6wwh6t8Hc686) using Ansible Vault.

### Encryption:

* Run the following command in your terminal:

```
ansible-vault encrypt_string '5sx4G3nbHG4krwjj7w7wqQwrndr63b' --name 'fortigate_token'
```

* This will prompt you to create a password. Once you enter and confirm the password, it will output an encrypted string.

example. 
```
Encryption successful

fortigate_token: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          38323665323035666633396461383536386233336461613036323834363161383863393637333162
          3737393364363030663063303730393862613434613333380a346665656432633936373266333831
          63613033633362373438633965666265323837313832666633383537363161653065396532613332
          3939386435393335300a336530343034616265613833306564663431396435653265653630313236
          39613266386438623266623462326437323765316339323665326336613535343462
```
## Storing Encrypted Token:

Copy the encrypted string.
Save it in a YAML file (e.g., secrets.yml) or directly in your Ansible playbook.
